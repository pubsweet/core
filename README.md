# Warning: Deprecated

This code is no longer used in PubSweet. Its functionality has been replaced by individual [pubsweet](https://gitlab.coko.foundation/pubsweet/pubsweet), [pubsweet-frontend](https://gitlab.coko.foundation/pubsweet/pubsweet-frontend) and [pubsweet-backend](https://gitlab.coko.foundation/pubsweet/pubsweet-backend) modules.

We will soon archive this repository.